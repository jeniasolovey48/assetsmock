﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AssetsMock.Controllers
{
    [ApiController]
    [Route("v1/acme/assets")]
    public class AssetsMockController : ControllerBase
    {
        [HttpGet]
        public Response Get([FromQuery] List<int> assetIds)
        {
            var rng = new Random();

            List<int> ids = null;
            
            ids = assetIds.Count == 0 ? Enumerable.Range(1, 100).ToList() : assetIds;

            return new Response()
            {
                items = ids.Select(index => new AssetResource
                    {
                        id = index,
                        category = rng.Next().ToString(),
                        serialNumber = rng.Next().ToString(),
                        isDeleted = false
                    })
                    .ToArray()
            };
        }
    }

    public class Response
    {
        public AssetResource[] items { get; set; }
    }
    
    public class AssetResource
    {
        public int id { get; set; }
        public string serialNumber { get; set; }
        public string category { get; set; }
        public bool isDeleted { get; set; }
    }
}